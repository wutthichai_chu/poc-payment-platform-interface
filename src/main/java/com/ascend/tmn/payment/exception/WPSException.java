package com.ascend.tmn.payment.exception;

/**
 * Created by wutthichai.chu on 5/16/17.
 */
public class WPSException extends Exception {
    public static final String NAMESPACE = "Wallet Payment Service";
    private int code;

    public WPSException(String message, int code) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
