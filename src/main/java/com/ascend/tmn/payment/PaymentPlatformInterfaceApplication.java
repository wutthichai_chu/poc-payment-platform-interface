package com.ascend.tmn.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentPlatformInterfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentPlatformInterfaceApplication.class, args);
	}
}
