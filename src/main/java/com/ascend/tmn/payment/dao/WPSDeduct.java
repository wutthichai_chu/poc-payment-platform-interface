package com.ascend.tmn.payment.dao;

import com.ascend.tmn.payment.domain.PaymentRequest;
import com.ascend.tmn.payment.domain.PaymentResponse;
import com.ascend.tmn.payment.domain.WPSDeductResponse;
import com.ascend.tmn.payment.exception.WPSException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by wutthichai.chu on 5/16/17.
 */
public class WPSDeduct implements Deduct {

    @Autowired
    private RestTemplate restTemplate;

    private final static String URL = "http://10.221.8.133:8080/wps/v1/deducts";

    public PaymentResponse makePayment(PaymentRequest paymentRequest) throws WPSException {
        //Build Header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        //Build Body
        MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();
        form.add("id", paymentRequest.getId());
        form.add("customer", paymentRequest.getCustomer());
        form.add("im", paymentRequest.getIm());
        form.add("amount", paymentRequest.getAmount().toString());
        form.add("channel", String.valueOf(paymentRequest.getChannel()));
        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(form, headers);

        WPSDeductResponse deductResponse = restTemplate.postForObject(URL, httpEntity, WPSDeductResponse.class);
        PaymentResponse response = null;
        if(deductResponse.getCode().equals("0")) {
            response = new PaymentResponse(String.valueOf(deductResponse.getTransactionId()), deductResponse.getCurrentBalance());
        } else {
            throw new WPSException(deductResponse.getDesc(), Integer.parseInt(deductResponse.getCode()));
        }
        return response;
    }
}
