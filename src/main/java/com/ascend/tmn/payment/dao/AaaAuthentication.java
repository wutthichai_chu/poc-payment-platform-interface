package com.ascend.tmn.payment.dao;

import com.ascend.tmn.payment.domain.AaaMerchantProfile;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

/**
 * Created by wutthichai.chu on 5/19/17.
 */
@Service
public class AaaAuthentication {
    @Autowired
    private RestTemplate restTemplate;

    private final static String URL = "http://alpha-mockserver.tmn-dev.com:10065/mcapi/merchant";

    public AaaMerchantProfile auth(String username, String password) {

        String credential = username + ":" + password;
        byte[] encodedCredential = Base64.encodeBase64(credential.getBytes(Charset.forName("US-ASCII")) );

        //Build Header
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + new String(encodedCredential));

        //Build Body
        HttpEntity<MultiValueMap> entity = new HttpEntity<>(headers);
        ResponseEntity<AaaMerchantProfile> response = restTemplate.exchange(URL, HttpMethod.GET, entity, AaaMerchantProfile.class);
        return response.getBody();
    }
}
