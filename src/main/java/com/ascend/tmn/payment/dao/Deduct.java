package com.ascend.tmn.payment.dao;

import com.ascend.tmn.payment.domain.PaymentRequest;
import com.ascend.tmn.payment.domain.PaymentResponse;

/**
 * Created by wutthichai.chu on 5/15/17.
 */
public interface Deduct {
    PaymentResponse makePayment(PaymentRequest paymentRequest) throws Exception;
}
