package com.ascend.tmn.payment.controller;

import com.ascend.tmn.payment.domain.AaaMerchantProfile;
import com.ascend.tmn.payment.domain.PaymentRequest;
import com.ascend.tmn.payment.domain.PaymentResponse;
import com.ascend.tmn.payment.service.DeductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * Created by wutthichai.chu on 5/15/17.
 */
@RestController
@RequestMapping("/v1/par")
public class PayAtRetailController {

    @Autowired
    private DeductService deductService;

    @ApiOperation(value = "payment", authorizations = {@Authorization(value="AAA")})
    @RequestMapping(value = "/payment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PaymentResponse payment(
            @RequestParam String id,
            @RequestParam String customer,
            @RequestParam BigDecimal amount,
            @RequestParam int channel) throws Exception {
        AaaMerchantProfile merchantProfile = (AaaMerchantProfile) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PaymentRequest paymentRequest = new PaymentRequest(id, customer, merchantProfile.getIm(), amount, channel);
        PaymentResponse paymentResponse = deductService.makePayment(paymentRequest);
        return paymentResponse;
    }
}
