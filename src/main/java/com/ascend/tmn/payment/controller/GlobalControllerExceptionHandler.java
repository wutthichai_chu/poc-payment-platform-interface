package com.ascend.tmn.payment.controller;

import com.ascend.tmn.payment.domain.ErrorResponse;
import com.ascend.tmn.payment.exception.WPSException;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by wutthichai.chu on 5/16/17.
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(WPSException.class)
    public ResponseEntity<ErrorResponse> defaultErrorHandler(HttpServletRequest req, WPSException e) {
        ErrorResponse res = new ErrorResponse(e.getCode(), e.NAMESPACE, e.getMessage());
        return new ResponseEntity<ErrorResponse>(res, HttpStatus.BAD_REQUEST);
    }
}
