package com.ascend.tmn.payment.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by wutthichai.chu on 5/19/17.
 */
public class AaaMerchantProfile {
    @JsonProperty("display_name")
    private String displayName;
    private String username;
    private String email;
    private String im;
    private String code;
    private String detail;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
