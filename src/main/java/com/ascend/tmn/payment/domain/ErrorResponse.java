package com.ascend.tmn.payment.domain;

/**
 * Created by wutthichai.chu on 5/16/17.
 */
public class ErrorResponse {

    private int code;
    private String namespace;
    private String message;

    public ErrorResponse(int code, String namespace, String message) {
        this.code = code;
        this.namespace = namespace;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
