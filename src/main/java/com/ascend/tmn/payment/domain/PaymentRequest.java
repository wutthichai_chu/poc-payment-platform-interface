package com.ascend.tmn.payment.domain;

import java.math.BigDecimal;

/**
 * Created by wutthichai.chu on 5/16/17.
 */
public class PaymentRequest {
    private String id;
    private String customer;
    private String im;
    private BigDecimal amount;
    private int channel;

    public PaymentRequest(String id, String customer, String im, BigDecimal amount, int channel) {
        this.id = id;
        this.customer = customer;
        this.im = im;
        this.amount = amount;
        this.channel = channel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "PaymentRequest{" +
                "id='" + id + '\'' +
                ", customer='" + customer + '\'' +
                ", im='" + im + '\'' +
                ", amount=" + amount +
                ", channel=" + channel +
                '}';
    }
}
