package com.ascend.tmn.payment.domain;

import java.math.BigDecimal;

/**
 * Created by wutthichai.chu on 5/15/17.
 */
public class PaymentResponse {

    private String transaction_id;
    private BigDecimal current_balance;

    public PaymentResponse(String transaction_id, BigDecimal current_balance) {
        this.transaction_id = transaction_id;
        this.current_balance = current_balance;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public BigDecimal getCurrent_balance() {
        return current_balance;
    }

    public void setCurrent_balance(BigDecimal current_balance) {
        this.current_balance = current_balance;
    }

    @Override
    public String toString() {
        return "PaymentResponse{" +
                "transaction_id='" + transaction_id + '\'' +
                ", current_balance=" + current_balance +
                '}';
    }
}
