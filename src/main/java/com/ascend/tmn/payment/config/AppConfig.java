package com.ascend.tmn.payment.config;

import com.ascend.tmn.payment.dao.Deduct;
import com.ascend.tmn.payment.dao.WPSDeduct;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wutthichai.chu on 5/16/17.
 */
@Configuration
public class AppConfig {

    @Bean
    public Deduct deduct() {
        return new WPSDeduct();
    }

}
