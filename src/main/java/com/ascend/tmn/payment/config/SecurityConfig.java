package com.ascend.tmn.payment.config;

import com.ascend.tmn.payment.security.AaaAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Created by wutthichai.chu on 5/19/17.
 */
@Configuration
@EnableWebSecurity
@ComponentScan("com.ascend.tmn.payment.security")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AaaAuthenticationProvider authProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.authenticationProvider(authProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests().antMatchers("/v1/par/payment").authenticated()
            .and().httpBasic();
    }
}
