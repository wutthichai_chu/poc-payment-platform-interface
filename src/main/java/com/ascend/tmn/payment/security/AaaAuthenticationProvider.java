package com.ascend.tmn.payment.security;

import com.ascend.tmn.payment.dao.AaaAuthentication;
import com.ascend.tmn.payment.domain.AaaMerchantProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by wutthichai.chu on 5/19/17.
 */
@Component
public class AaaAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private AaaAuthentication aaaAuthentication;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        AaaMerchantProfile merchantProfile = aaaAuthentication.auth(username, password);
        if (merchantProfile instanceof AaaMerchantProfile && !isEmpty(merchantProfile.getIm())) {
            Authentication user = new UsernamePasswordAuthenticationToken(merchantProfile, password, new ArrayList<>());
            return user;
        }
        return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
