package com.ascend.tmn.payment.service;

import com.ascend.tmn.payment.dao.Deduct;
import com.ascend.tmn.payment.domain.PaymentRequest;
import com.ascend.tmn.payment.domain.PaymentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wutthichai.chu on 5/16/17.
 */
@Service
public class DeductService {

    @Autowired
    private Deduct deduct;

    public PaymentResponse makePayment(PaymentRequest paymentRequest) throws Exception{
        return deduct.makePayment(paymentRequest);
    }
}
